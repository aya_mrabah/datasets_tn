J'ai fait  la page discussion celle ci contient Wzb.html , style_ons.css.
Grace a cette page les utilisateurs peuvent discuter des different sujets sientifique consernant le machine learning . 
//////////////////////LE FIcHIER HTML /////////////////////////////////
 
 <!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Disscussion</title>
      <link rel ="stylesheet" href= "style_ons.css"/> 

        <script src="https://kit.fontawesome.com/45e38e596f.js" crossorigin="anonymous"></script>

  </head>

 <body>
      <nav>
           <img src="dataset.png" height="150" alt="logo">
          <div id="acc">
            <a href="">HOME &nbsp &nbsp  </a>
            <a href="">DATASETS &nbsp &nbsp  </a>
            <a href="wzb.html">DISCUSSION  </a>
           </div>
        </nav>
        <article>
            <img src="datadisc.png" alt="pic">
            <p> 
                <br>
              Discuss the dataset platform topics this includes <br>
               sharing feedback, asking questions, and more.
           </p>
            
            <!-- search box -->
            <div class="wrap">
                <div class="search">
                   <input type="text" class="searchTerm" placeholder="Search">
                   <button type="submit" class="searchButton">
                     <i class="fa fa-search"></i>
                  </button>
                </div>
            </div>
        </article>
        <!-- tableau-->
        <table class="styled-table">
          <thead>
              <tr>
                  <th>Forums</th>
                  <th>Discussions</th>
                  <th>Messages</th>
                  <th>Last Message</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td>Computer Science</td>
                  <td>3 675</td>
                  <td> 8 058</td>
                  <td>07-11-2021 11:21:11<br>
                      from Aya</td>
              </tr>
              <tr class="active-row">
                  <td>Computer Vision</td>
                  <td>1 067	</td>
                  <td> 10 663	</td>
                  <td>01-11-2021 18:01:02  <br>
                      from Ali</td> 
              </tr>
              <tr>
                  <td>Data Visualization </td>
                  <td>119</td>
                  <td>463</td>
                  <td>Yesterday 19:56:15 <br>
                      from Ousema</td>
              </tr>
              <tr class="active-row">
                  <td>Classification </td>
                  <td>744</td>
                  <td> 471</td>
                  <td>22-10-2021 22:36:45 <br>
                      from Souheil
                  </td> 
              </tr>
              <tr>
                  <td>NLP</td>
                  <td>322</td>
                  <td>354</td>
                  <td>09-11-2021 20:19:00 <br>
                      from salwa</td>
              </tr>
              <tr class="active-row">
                  <td>Regression </td>
                  <td>112</td>
                  <td>134</td>
                  <td>17-08-2021 15:12:24 <br>
                      from mostapha 
                  </td> 
              </tr>
              
          </tbody>
        </table> 

        <div class="p3">
            <h1><i class="fas fa-users"></i> &nbsp &nbsp Discussion from across Dataset.tn</h1>
            <a href="">[Up-to-data Summary] We passed 30% of tileline, these are the stories up to this point!</a>
            <p>&nbsp &nbsp Nejib Bouslema</p>
            <hr>
           
            <a href="">Meme Thread </a>
            <p> &nbsp &nbsp Salma Ouerteni</p>
            <hr>
            
            <a href="">The Python Graph Gallery: Learning </a>
            <p>&nbsp &nbsp Naceur khelifi</p>
            <hr>
           
            <a href="">I’m worried this comp will turn out to be RNG based </a>
            <p>&nbsp &nbsp Ben Haj Larbi</p>
        </div>
            
   
    </body>
</html>


//////////////////////LA PAGE CSS//////////////////////////////////////////////////


nav img{
    float :left ;
    
}
nav a {
   
     font-size: 25px;
     text-decoration: none;
     font-family:Georgia, 'Times New Roman', Times, serif;
     
}


#acc
{
    margin-left: 500px;
}
article
{
    background-color: rgba(141, 18, 18, 1);
    margin-top: 130px;
    height: 155px;
}
article img{
    float:left;
}
article p{
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-size: 24px;
    color: rgb(255, 255, 255);
    margin-left: 380px;
}

/*+++++search box ++++++++++++*/
.search {
    width: 100%;
    position: relative;
    display: flex;
  }
  
  .searchTerm {
    width: 100%;
    border: 3px solid #000000;
    border-right: none;
    padding: 5px;
    height: 20px;
    border-radius: 5px 0 0 5px;
    outline: none;
    color: #000000;
  }
  
  .searchTerm:focus{
    color: #a52222;
  }
  
  .searchButton {
    width: 40px;
    height: 36px;
    border: 3px solid #000000;
    background: #aeb9bb;
    text-align: center;
    color: rgb(10, 10, 10);
    border-radius: 0 5px 5px 0;
    cursor: pointer;
    font-size: 20px;
  }
  
  /*Resize the wrap to see the search bar change!*/
  .wrap{
    width: 30%;
    position: absolute;
    top: 44%;
    left: 50%;
    margin-left: 450px;
    transform: translate(-50%, -50%);
    
  }
/*++++++++++ le tableau +++++++++++ */
.styled-table {
    border-collapse: collapse;
    margin: 60px 60px;
    
    font-size: 18px;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    width: 90%;
    /*min-width: 400px;*/
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}

.styled-table thead tr {
    background-color: rgba(141, 18, 18, 1);
    color: #ffffff;
    text-align: center;
    font-size: 18px;
}

.styled-table th,
.styled-table td {
    padding: 12px 15px;
}
.styled-table tbody tr {
    border-bottom: 1px solid #dddddd;
    font-weight: bold;
    text-align: center;
}

.styled-table tbody tr:nth-of-type(even) {
    background-color: #f3f3f3;
}

.styled-table tbody tr:last-of-type {
    border-bottom: 2px solid rgba(141, 18, 18, 1);
    
}
.styled-table tbody tr.active-row {
    font-weight: bold;
    text-align: center; 
    color:rgba(141, 18, 18, 1);
}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
.p3 a{
    text-decoration: none;
    font-size: 22px;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    color: black;
    font-weight: bold;
}
.p3 p{
    font-size: 20px;
    font-style: italic;
    color: rgb(90, 83, 83);
    font-family: roboto;
}
.p3 hr{
    background-color :rgba(141, 18, 18, 1);
    height: 5px;
}